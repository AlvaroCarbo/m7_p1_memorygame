package com.example.m78_p1_memorygame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Debug
import android.os.SystemClock
import android.view.View
import android.widget.Button
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class DifficultGameActivity : AppCompatActivity(), View.OnClickListener {
    // UI Views
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var carta7: ImageView
    private lateinit var carta8: ImageView
    private lateinit var resetButton: Button
    private lateinit var endButton: Button
    private lateinit var simpleChronometer: Chronometer

    private lateinit var buttons: List<ImageView>

    private lateinit var movementsText: TextView


    // ViewModel declaration
    private lateinit var viewModel: DifficultViewActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.difficult_screen)

        viewModel = ViewModelProvider(this).get(DifficultViewActivity::class.java)

        carta1 = findViewById(R.id.carta1)
        carta2 = findViewById(R.id.carta2)
        carta3 = findViewById(R.id.carta3)
        carta4 = findViewById(R.id.carta4)
        carta5 = findViewById(R.id.carta5)
        carta6 = findViewById(R.id.carta6)
        carta7 = findViewById(R.id.carta7)
        carta8 = findViewById(R.id.carta8)
        resetButton = findViewById(R.id.resetButton)
        endButton = findViewById(R.id.endButton)
        movementsText = findViewById(R.id.movements)


        movementsText.text = "Total Moves: " + viewModel.tapsTotals.toString()

        buttons = listOf(carta1, carta2, carta3, carta4, carta5, carta6, carta7, carta8)

        for (carta in buttons) {
            carta.setOnClickListener(this)
        }

        simpleChronometer = findViewById(R.id.simpleChronometer) // initiate a chronometer
        simpleChronometer.base = viewModel.chronometerTime
        simpleChronometer.start() // start a chronometer

        resetButton.setOnClickListener {
            viewModel.resetEstatJoc()
            viewModel.barrejarCartes()

            updateUI()
            viewModel.tapsTotals = 0
            movementsText.text = "Total Moves: " + viewModel.tapsTotals.toString()
            simpleChronometer.base = SystemClock.elapsedRealtime()
            simpleChronometer.start()
            for (button in buttons) {
                button.alpha = 1F
            }
        }

        endButton.setOnClickListener {
            val intent = Intent(this@DifficultGameActivity, MainActivity::class.java)
            startActivity(intent)
        }

        for (i in 0..7){
            if (viewModel.cartaEncertada(i))
                buttons[i].alpha = 0.5F
        }

        updateUI()
    }

    override fun onClick(v: View?) {
        when (v) {
            carta1 -> girarCarta(0, carta1)
            carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3)
            carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5)
            carta6 -> girarCarta(5, carta6)
            carta7 -> girarCarta(6, carta7)
            carta8 -> girarCarta(7, carta8)
        }
    }

    // Funció que utilitzarem per girar la carta
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        if (!viewModel.cartaEncertada(idCarta)){
            viewModel.tapsTotals++
            viewModel.cartesGirades++
            movementsText.text = "Total Moves: " + viewModel.tapsTotals.toString()


            viewModel.chronometerTime = simpleChronometer.base
            if (viewModel.cartesGirades > 2) {
                viewModel.cartesGirades = 1
                viewModel.resetEstatJoc()
                updateUI()
            }

            carta.setImageResource(viewModel.girarCarta(idCarta))

            if (viewModel.parellaEncertada(idCarta)) {
                buttons[idCarta].alpha = 0.5F
                buttons[viewModel.primeraEncertada(idCarta)].alpha = 0.5F
            }

            if (viewModel.jocFinalitzat()) {
                simpleChronometer.stop()
                val intent = Intent(this@DifficultGameActivity, ResultActivity::class.java)
                intent.putExtra("time", SystemClock.elapsedRealtime() - simpleChronometer.base)
                intent.putExtra("taps", viewModel.tapsTotals)
                intent.putExtra("difficulty", viewModel.modeDeJoc())
                startActivity(intent)
            }
            viewModel.estatJoc()
        }
    }


    // Funció que restauarà l'estat de la UI
    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0))
        carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2))
        carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4))
        carta6.setImageResource(viewModel.estatCarta(5))
        carta7.setImageResource(viewModel.estatCarta(6))
        carta8.setImageResource(viewModel.estatCarta(7))

    }
}









