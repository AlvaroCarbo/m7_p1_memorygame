package com.example.m78_p1_memorygame

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.concurrent.TimeUnit


class ResultActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.result_screen)
        val extras = intent.extras
        val exitButton = findViewById<Button>(R.id.menuButton)
        val playAgainButton = findViewById<Button>(R.id.playAgainButton)
        if (extras != null) {
            val taps = extras.getInt("taps")
            val time = extras.getLong("time")
            val seconds = TimeUnit.MILLISECONDS.toSeconds(time)
            val score = findViewById<TextView>(R.id.title)
            val timeText = findViewById<TextView>(R.id.time)
            val tapsText = findViewById<TextView>(R.id.taps)
            score.text = "Your score\n" + (1000000 / (seconds*taps))
            timeText.text = "You have lasted\n$seconds seconds"
            tapsText.text = "You have done\n$taps taps"

            //difficutly = extras.getInt("difficulty")


            //textResult.text = "Your Score is: " + (1000000 / (seconds*taps)) + "\nYou have lasted: " + seconds + " seconds." + "\nYou have done: " + taps + " taps."
            //The key argument here must match that used in the other activity
        }

        exitButton.setOnClickListener {
            val intent = Intent(this@ResultActivity, MainActivity::class.java)
            startActivity(intent)
        }

        playAgainButton.setOnClickListener {
            var intent: Intent
            val difficutly = extras?.getInt("difficulty")

            intent = if (difficutly == 8) {
                Intent(this@ResultActivity, DifficultGameActivity::class.java)
            } else {
                Intent(this@ResultActivity, GameActivity::class.java)
            }

            startActivity(intent)
        }
    }
}