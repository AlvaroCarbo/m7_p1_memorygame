package com.example.m78_p1_memorygame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import android.content.DialogInterface





class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_M78P1memoryGame)
        setContentView(R.layout.activity_main)

        val spinner = findViewById<Spinner>(R.id.difficultySpinner)
        val difficulty = resources.getStringArray(R.array.Difficulty)
        val playButton =  findViewById<Button>(R.id.playButton)
        val helpButton = findViewById<Button>(R.id.helpButton)
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, difficulty)
        spinner.adapter = adapter

        helpButton.setOnClickListener {

            val builder = AlertDialog.Builder(this)
            //set title for alert dialog

            builder.setTitle(R.string.dialogTitle)
            //set message for alert dialog
            builder.setMessage(R.string.dialogMessage)
            builder.setIcon(R.drawable.pok__ball_icon)

            //performing Ok action
            builder.setNeutralButton("Ok"){dialogInterface , which ->
                dialogInterface.cancel()
            }

            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }



        playButton.setOnClickListener {
            if (spinner.selectedItem.toString() == "Normal") {
                val intent = Intent(this@MainActivity, GameActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this@MainActivity, DifficultGameActivity::class.java)
                startActivity(intent)
            }
        }
        

        // access the spinner
        if (spinner != null) {
            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View?, position: Int, id: Long) {
                    Log.i("Spinner value: ", spinner.selectedItem.toString())

                    Toast.makeText(this@MainActivity,
                        getString(R.string.selected_item) + " " +
                                "" + difficulty[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }
}