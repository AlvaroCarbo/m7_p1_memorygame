package com.example.m78_p1_memorygame

import android.os.SystemClock
import android.util.Log
import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {
    // Game data model
    var imatges = arrayOf(
        R.drawable.card1,
        R.drawable.card2,
        R.drawable.card3,
        R.drawable.card1,
        R.drawable.card2,
        R.drawable.card3
    )
    var cartes = mutableListOf<Carta>()

    // Aquesta funció es estàndard de cada classe, i és com el constructor
    var cartesGirades: Int = 0

    var chronometerTime: Long = SystemClock.elapsedRealtime()

    var tapsTotals: Int = 0


    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges.shuffle()
        for (i in 0..5) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int) : Int {
        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true
            return cartes[idCarta].resId
        } else {
            cartes[idCarta].girada = false
            return R.drawable.back_cards
        }
    }

    // Funció que posa l'estat del joc al mode inicial
    fun resetEstatJoc() {
        for (i in 0..5) {
            cartes[i].girada = false
        }
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        return if (cartes[idCarta].girada || cartes[idCarta].encertada) {
            cartes[idCarta].resId
        } else {
            R.drawable.back_cards
        }
    }

    fun cartaEncertada(idCarta: Int) : Boolean {
        return cartes[idCarta].encertada
    }

    fun estatJoc() {
        for (i in 0..5) {
            Log.i(cartes[i].toString(), cartes[i].resId.toString())
        }
    }

    fun parellaEncertada(idCarta: Int) : Boolean {
        for (i in 0..5) {
            if ((cartes[i].id != cartes[idCarta].id) && (cartes[i].girada && cartes[idCarta].girada) && (cartes[i].resId == cartes[idCarta].resId)) {
                cartes[i].encertada = true
                cartes[idCarta].encertada = true
                return true
            }
        }
        return false
    }

    fun primeraEncertada(idCarta: Int): Int {
        for (i in 0..5) {
            if ((cartes[i].id != cartes[idCarta].id) && (cartes[i].girada && cartes[idCarta].girada) && (cartes[i].resId == cartes[idCarta].resId)) {
                return i
            }
        }
        return 0
    }

    fun barrejarCartes() {
        cartes.clear()
        imatges.shuffle()
        for (i in 0..5)
            cartes.add(Carta(i, imatges[i]))
    }

    fun jocFinalitzat(): Boolean {
        var contador = 0
        for (i in 0..5) {
            if (cartes[i].encertada) {
                contador++
            }
        }
        return contador == 6
    }

    fun modeDeJoc(): Int {
        var contador = 0
        for (i in 0..5) {
            if (cartes[i].encertada) {
                contador++
            }
        }
        return contador
    }

}



