package com.example.m78_p1_memorygame

data class Carta(val id: Int, val resId: Int, var girada: Boolean = false, var encertada : Boolean = false)